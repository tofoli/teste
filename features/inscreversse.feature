# language: pt

Funcionalidade: Criar conta
  Para poder fazer publicações e comentários
  Quero poder escolher um usuario e uma senha

  Cenário: Cadastro de nova conta com sucesso

    Quando preencho os campos Usuario com "teste", Senha com "t35t3PW3" e clico em cadastrar
    Então vejo a seguinte mensagem na tela:
      """
      Bem Vindo ao Blog! Conta cadastrada com sucesso o/
      """

    Dado que preencho os campos Usuario com "teste", Senha com "t36t@=#?" e clico em cadastrar
    Quando preencho os campos Usuario com "teste", Senha com "t36t@=#?" e clico em cadastrar
    Então vejo a seguinte mensagem na tela:
      """
      Usuário indisponível!
      """

    Quando preencho os campos Usuario com "teste", Senha com "1234" e clico em cadastrar
    Então vejo a seguinte mensagem na tela:
      """
      Senha muito fraca!
      """

    Quando preencho os campos Usuario com "teste", Senha com "234jisdn21", confirmação de senha com "as4huqwd" e clico em cadastrar
    Então vejo a seguinte mensagem na tela:
      """
      As senhas não conferem!
      """