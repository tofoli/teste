When(/^preencho os campos Usuario com "([^"]*)", Senha com "([^"]*)" e clico em cadastrar$/) do |user, password|
  fill_form(user, password)
end

When(/^que preencho os campos Usuario com "([^"]*)", Senha com "([^"]*)" e clico em cadastrar$/) do |user, password|
  fill_form(user, password)
end

When(/^vejo a seguinte mensagem na tela:$/) do |text|
  page.has_content? text
end

When(/^preencho os campos Usuario com "([^"]*)", Senha com "([^"]*)", confirmação de senha com "([^"]*)" e clico em cadastrar$/) do |user, password, confirm_password|
  fill_form(user, password, confirm_password)
end
