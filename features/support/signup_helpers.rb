module SignupHelpers
  def fill_form(user, password, confirm_password=password)
    visit('/signup')

    fill_in 'User',     with: user
    fill_in 'Password', with: password
    fill_in 'Confirm',  with: confirm_password

    click_button 'Create Account'
  end
end

World(SignupHelpers)
