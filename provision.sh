#! /usr/bin/env bash

# Variables
apt-get update
apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev nodejs

cd /opt/
git clone https://github.com/rbenv/rbenv.git /opt/rbenv
echo 'export PATH="/opt/rbenv/bin:$PATH"' >> /etc/bash.bashrc
echo 'eval "$(rbenv init -)"' >> /etc/bash.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git /opt/rbenv/plugins/ruby-build
echo 'export PATH="/opt/rbenv/plugins/ruby-build/bin:$PATH"' >> /etc/bash.bashrc
exec $SHELL

rbenv install 1.9.2-p330
rbenv global 1.9.2-p330

echo -e "** Visit http://localhost:8080 in your browser for to view the application **"
